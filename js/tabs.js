$tabs = {
    "create": url => {
        return new Promise((resolve, reject) => {
            chrome.tabs.create({'url': url}, tab => resolve(tab));
        });
    },
    "close": id => {
        return new Promise((resolve, reject) => {
            chrome.tabs.remove(id, () => resolve(true));
        });
    },
    "request_all": () => {
        return new Promise((resolve, reject) => {
            chrome.tabs.getAllInWindow(null, tabs => resolve(tabs));
        });
    },
    "get": id_or_ids => {
        if (typeof(id_or_ids) == 'number') {
            var id = id_or_ids;
            return new Promise((resolve, reject) => {
                chrome.tabs.get(id, (tab) => resolve(tab));
            });
        } else {
            var ids = id_or_ids;
            return Promise.all(ids.map($tabs.get));
        }
    }
};

function tabs_filter(tab) {
    return tab.url != 'chrome://newtab/';
}

function render_tabs(tabs) {
    var tabs_wrapper = $('[role="tabs"]');

    var filtered_tabs = tabs.filter(tabs_filter);
    tabs_wrapper.attr('full', filtered_tabs.length == tabs.length);

    var container = tabs_wrapper.find('.cb-list')[0];
    container.innerHTML = ''; // remove old content

    function tab_entry(tab) {
        // Data
        var tid = "tab_" + tab.id;
        var title = escape_html(tab.title);

        // Pieces
        var li = $('<li>', {
            title: title
        })[0];
        var inp = $('<input>', {
            id: tid,
            type: "checkbox"
        })[0];
        var lbl = $('<label>', {
            for: tid
        })[0];
        if (tab.favIconUrl) {
            var img = $('<img>', {
                src: tab.favIconUrl
            }).error(no_inet_icon)[0];
            lbl.appendChild(img);
        }
        var span = $('<span>', {
            text: title
        })[0];
        lbl.appendChild(span);

        li.appendChild(inp);
        li.appendChild(lbl);

        return li;
    }

    var all = tab_entry({
        id: "all",
        title: "All"
    });
    all.setAttribute('role', "all");
    container.appendChild(all);
    for (tab of filtered_tabs) {
        container.appendChild(tab_entry(tab));
    }
    activate_cb_list(container);
}
