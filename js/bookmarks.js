$bookmarks = {
    "request_full_tree": () => {
        return new Promise((resolve, reject) => {
            chrome.bookmarks.getTree((bm_tree) => {
                resolve(bm_tree[0]);
            });
        });
    },
    "get": id_or_ids => {
        if (typeof(id_or_ids) == 'string') {
            var id = id_or_ids;
            return new Promise((resolve, reject) => {
                chrome.bookmarks.get(id, bm => resolve(bm));
            });
        } else {
            var ids = id_or_ids;
            return Promise.all(ids.map($bookmarks.get));
        }
    },
    "search_by_path": path => {
        var components = path.split('/');
        return $bookmarks.request_full_tree()
            .then(root => {
                var current_layer = root;
                for (title_part of components) {
                    var found = false;
                    for (child of current_layer.children) {
                        if (child.title == title_part) {
                            current_layer = child;
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        throw new Error('Search failed');
                    }
                }
                return current_layer;
            });
    },
    "get_children": id => {
        return new Promise((resolve, reject) => {
            chrome.bookmarks.getChildren(id, (bm_nodes) => {
                resolve(bm_nodes);
            });
        });
    },
    "create": (parent_id, title, url) => {
        return new Promise((resolve, reject) => {
            chrome.bookmarks.create({
                'parentId': parent_id,
                'title': title,
                'url': url
            }, bm => resolve(bm))
        });
    },
    "remove_tree": id => {
        return new Promise((resolve, reject) => {
            chrome.bookmarks.removeTree(id, resolve);
        });
    }
};
