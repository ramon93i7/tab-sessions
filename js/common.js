function escape_html(input) {
    return $('<i>').text(input).html();
}

$background_page = () => {
    return new Promise((resolve, reject) => {
        chrome.runtime.getBackgroundPage(page => resolve(page));
    });
};

jQuery.fn.on_clicks = function(on_click, on_dblclick, timeout) {
    return this.each((_, x) => {
        var clicks = 0, self = this;
        $(x).click((event) => {
            clicks++;
            if (clicks == 1) {
                setTimeout(() => {
                    var chosen = (clicks == 1) ? on_click : on_dblclick;
                    chosen.call(self, event);
                    clicks = 0;
                }, timeout || 200);
            }
        });
    });
};
