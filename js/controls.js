/* COMMON */
function show_menu() {
    $('#face').addClass('active');
    $('#face').focus();
}

function hide_menu() {
    $('#face').removeClass('active');
    $face_menu_keybindings.reset();
}

function return_to_menu(event) {
    $active.removeClass('active');
    show_menu();
}

function show(block_id, header_role) {
    var selector = [];
    var block = '#' + block_id;
    selector.push(block);
    selector.push([block, '.header[role="' + header_role + '"]'].join(' '));
    selector.push([block, '.body'].join(' '));
    $active = $(selector.join(', ')).addClass('active');
    $(block).focus();
}

/* FACE MENU STRUCTURE */
function init_face_menu_keybindings() {
    var LEFT = 37, UP = 38, RIGHT = 39, DOWN = 40;
    var face = $('#face');
    var btns = face.find('.menu-button,.menu-dual-button');

    var root = {
        'elem': null
    };
    var cur_node = root;
    var cur_sub_node = null;

    for (var btn of btns.map((_, x) => $(x))) {
        if (!btn.hasClass('disabled')) {
            var node = {};
            node[UP] = cur_node;
            cur_node[DOWN] = node;
            if (cur_sub_node) {
                cur_sub_node[DOWN] = node;
                cur_sub_node = null;
            }
            if (btn.hasClass('menu-button')) {
                node.elem = btn[0];
            } else {
                // Tune sub-node
                var sub_node = {};
                sub_node.elem = btn.find('[role="sub"]')[0];
                sub_node[LEFT] = node;
                cur_sub_node = sub_node;
                // Tune node
                node.elem = btn.find('[role="main"]')[0];
                node[RIGHT] = sub_node;
            }
            cur_node = node;
        }
    }

    // Tune root
    root[DOWN][UP] = undefined; // remove link to root
    root[UP] = cur_node; // cycle link

    $root = root;

    // State & interface
    var state_node = root;
    $face_menu_keybindings = {};
    $face_menu_keybindings.reset = () => {
        state_node.elem && $(state_node.elem).toggleClass('hover');
        state_node = root;
    };
    $face_menu_keybindings.handle = (event) => {
        var ENTER = 13;
        var char_code = event.keyCode;
        var new_state = state_node[char_code];
        if (new_state !== undefined) {
            state_node.elem && $(state_node.elem).toggleClass('hover');
            $(new_state.elem).toggleClass('hover');
            state_node = new_state;
        } else if (char_code == ENTER) {
            state_node.elem && $(state_node.elem).click();
            $face_menu_keybindings.reset();
        }
    };
}

function face_menu_hovering(event) {
    $face_menu_keybindings.reset();
    $(event.currentTarget).toggleClass('hover');
}

/* FACE MENU ACTIONS */
function face_tab_save(event) {
    var trgt = $(event.currentTarget);
    var header_role = trgt.attr('id');

    hide_menu();
    $tabs.request_all()
        .then(render_tabs)
        .then(() => {
            show('tabs_block', header_role)
        });
}

function face_session_action(event) {
    hide_menu();
    get_session_folder()
        .then(render_sessions)
        .then(() => {
            show('sessions_block', $(event.currentTarget).attr('id'), true)
        });
}

/* ACCEPTERS */
function universal_accepter_on_enter(event) {
    if (event.keyCode == 13) {
        var header = $(event.currentTarget.parentNode.parentNode);
        var ok = header.find('[role="ok"]');
        ok.click();
    }
}

function universal_back_on_backspace(event) {
    var BACKSPACE = 8;
    var source = $(event.originalEvent.path[0]);
    if (event.keyCode == BACKSPACE && !source.is('input')) {
        event.preventDefault(); // Issue #5 fix
        var block = $(event.target);
        var back = block.find('[role="back"]');
        if (back.size()) {
            back.click();
        }
    }
}

function accept_session_action(event) {
    var header_role = $(event.currentTarget.parentNode.parentNode).attr('role');
    var session_header = $('#sessions_block [role="sessions"] [role="main"].active');
    var session_body = session_header.next();
    var id = session_header.attr('id').substr(3);
    session_action(header_role, id)
        .then(() => {
            if (header_role == 'delete') {
                /* Autofocus on nearest session */
                var next_header = session_header.prevUntil(null, 'li[role="main"]').first();
                if (!next_header.size()) {
                    next_header = session_header.nextUntil(null, 'li[role="main"]').first();
                }
                if (next_header.size()) {
                    next_header.addClass('active');
                }
                /***/
                [session_header, session_body].map(ss => ss.remove());
            } else {
                window.close();
            }
        });
}

function accept_tabs_action(event) {
    // Header data
    var header = $(event.currentTarget.parentNode.parentNode.parentNode);
    var is_closing = header.attr('role').startsWith('save_close');
    var name = escape_html(header.find('[role="value"]').val()).trim();
    // Body data
    var body = $('#tabs_block .body');
    var all_tabs = body.find('li[role="all"] input:checked').length == 1 && body.attr('full') == 'true';
    var tabs_ids = $.makeArray(body.find('li[role!="all"] input:checked').map((_, x) => x.id.substr(4) * 1));
    if (name.length > 0) {
        $tabs.get(tabs_ids)
            .then(tabs => create_session(name, tabs))
            .then(() => is_closing && Promise.all(
                (all_tabs && [$tabs.create('chrome://newtab')] || []).concat(tabs_ids.map($tabs.close))
            ))
            .then(window.close);
    }
}

// SESSION LIST MANIPULATION
function session_block_keybindings(event) {
    var ENTER = 13, DELETE = 46, UP = 38, DOWN = 40;
    var char_code = event.keyCode;
    var session_block = $(event.target);
    var header = session_block.find('.header.active');
    if (char_code !== undefined) {
        if (char_code == ENTER && header.size() && header.attr('role') == 'open' ||
            char_code == DELETE && header.size() && header.attr('role') == 'delete') {
            header.find('[role="ok"]').click();
        } else if (char_code == UP || char_code == DOWN) {
            var sessions = session_block.find('.body.active li[role="main"]');
            var active = sessions.filter('.active');
            if (!active.size()) {
                // Init selection on first/last session
                char_code == UP && sessions.last().addClass('active') ||
                    char_code == DOWN && sessions.first().addClass('active');
            } else {
                // Select prev/next session
                var new_active = undefined;
                if (char_code == UP) {
                    new_active = active.prevUntil(null, 'li[role="main"]').first();
                } else if (char_code == DOWN) {
                    new_active = active.nextUntil(null, 'li[role="main"]').first();
                }
                if (new_active.size()) {
                    active.removeClass('active');
                    new_active.addClass('active');
                }
            }
        } else {
            // Pass event to universal handler
            universal_back_on_backspace(event);
        }
    }
}
