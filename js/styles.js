function activate_cb_list(cb_list) {
    var cbl = $(cb_list);

    // All
    var r_all_inp = cbl.find('li[role="all"] input[type="checkbox"]');
    r_all_inp.on('change', function(event) {
        var all = event.target;
        var checked = all.checked;
        var deps = $(all.parentNode.parentNode).find("li[role!='all'] input[type='checkbox']");
        for (dep of deps) {
            dep.checked = checked;
        }
    });
    r_all_inp.click();

    // Main
    var r_nall = cbl.find('li[role!="all"]');
    r_nall.find('input[type="checkbox"]').on('change', function(event) {
        var trgt = event.target;
        var checked = trgt.checked;
        var par = $(trgt.parentNode.parentNode);
        var all = par.find('li[role="all"] input[type="checkbox"]')[0];
        if (checked) {
            for (li of par.find('li[role!="all"] input[type="checkbox"]')) {
                if (!li.checked) {
                    return;
                }
            }
            if (!all.checked) {
                all.checked = true;
            }
        } else {
            all.checked = false;
        }
    });


    var drag_obj = { start_element: undefined };

    r_nall.on('mousedown', function(event) {
        drag_obj.start_element = event.currentTarget;
        var li = $(event.currentTarget);
        var inp = li.find('input[type="checkbox"]');
        inp.click();
    });

    r_nall.on('mouseover', function(event) {
        if (drag_obj.start_element) {
            var li = $(event.currentTarget);
            var inp = li.find('input[type="checkbox"]');
            inp.click();
        }
    });

    r_nall.on('mouseup', function(event) {
        if (drag_obj.start_element) {
            if (drag_obj.start_element == event.currentTarget) {
                var li = $(event.currentTarget);
                var inp = li.find('input[type="checkbox"]');
                inp.click()
            }
            drag_obj.start_element = undefined;
        }
    });
}

function activate_nested_list(ns_list) {
    var nsl = $(ns_list);

    nsl.find('li[role="main"] + div li').each((idx, val) => {
        var rec = $(val);
        var img = $('<img>', {
            src: 'http://www.google.com/s2/favicons?domain_url=' + encodeURI(rec.attr('icon'))
        }).error(no_inet_icon);
        rec.find('label').before(img);
    });

    nsl.find('li[role="main"] > *').on_clicks(
        (event) => {
            var li = event.target.parentNode;
            var id = li.id;
            var par = li.parentNode;
            var segm = $(li);
            var was_active = segm.hasClass('active');

            // Deactivate all "main" lines
            $(par).find('li[role="main"]').removeClass('active');

            if (!was_active) {
                segm.addClass('active');
            }
        },
        (event) => {
            var li = event.target.parentNode;
            var segm = $(li);
            if (!segm.hasClass('active')) {
                segm.addClass('active');
            }

            $('#sessions_block [role="open"].header.active [role="ok"]').click();
        }
    );
}

function no_inet_icon(event) {
    var img = $(event.currentTarget);
    img.attr('src', "/icons/floppy16.png");
}
