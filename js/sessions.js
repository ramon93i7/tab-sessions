/* Sessions */

function iso_datetime() {
    var date = new Date();
    return (new Date(date.getTime() - date.getTimezoneOffset() * 6e4)).toISOString().substr(0, 19).replace('T', ' ');
}

function pack_session_info(name) {
    return ['[' + iso_datetime() + ']', name].join(' ');
}

function unpack_session_info(session_info) {
    return {
        'datetime': new Date(Date.parse(session_info.substr(1, 19))),
        'name': session_info.substr(22)
    }
}

function session_folder_path() {
    return localStorage.getItem('session_folder_path') || 'Other bookmarks';
}

function get_session_folder() {
    return $bookmarks.search_by_path([session_folder_path(), "#sessions"].join('/'));
}

function render_sessions(sessions) {
    var container = $('[role="sessions"] .nested-list')[0];
    container.innerHTML = ''; // remove old content

    function session_main_entry(session) {
        var li = $('<li>', {
            role: 'main',
            id: 'ss_' + session.id
        })[0];
        var session_info = unpack_session_info(escape_html(session.title));
        var div = $('<div>', {
            text: session_info.name
        })[0];

        li.appendChild(div);

        return li;
    }

    function session_record_entry(record) {
        var li = $('<li>', {
            icon: record.url
        })[0];
        var lbl = $('<label>', {
            text: escape_html(record.title)
        })[0];

        li.appendChild(lbl);

        return li;
    }

    for (session of sessions.children) {
        container.appendChild(session_main_entry(session));
        var div = $('<div>')[0];
        for (record of session.children) {
            div.appendChild(session_record_entry(record));
        }
        container.appendChild(div);
    }

    activate_nested_list(container);
}


/* MAIN */
function create_session(name, tabs) {
    var meta_name = pack_session_info(name);
    return get_session_folder()
        .then(sessions =>
            $bookmarks.create(sessions.id, meta_name)
        )
        .then(new_session_folder => [
            new_session_folder,
            Promise.all(tabs.map(
                tab => $bookmarks.create(
                    new_session_folder.id,
                    escape_html(tab.title),
                    tab.url
                )
            ))
        ]);
}

function session_action(action, id) {
    if (action == 'open') {
        return $bookmarks.get_children(id)
            .then((children) => {
                $background_page()
                    .then(page => {
                        page.create_tabs(children.map(child => child.url))
                    });
            });
    } else if (action == 'delete') {
        return $bookmarks.remove_tree(id);
    }
}
