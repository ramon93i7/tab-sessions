// Check session folder
$bookmarks.search_by_path([session_folder_path(), '#sessions'].join('/'))
    .then(show_menu)
    .catch(err => {
        //window.close()
        $bookmarks.search_by_path('Other bookmarks')
            .then(other_bm => {
                return $bookmarks.create(other_bm.id, '#sessions');
            })
            .then(show_menu);
    });


// Apply event handlers
setTimeout(() => {
    init_face_menu_keybindings();
    /* Click controls */
    $('#save_close, #save').on('click', face_tab_save);
    $('#open, #delete').on('click', face_session_action);
    $('.header [role="back"]').on('click', return_to_menu);
    $('#tabs_block .header [role="ok"]').on('click', accept_tabs_action);
    $('#sessions_block .header [role="ok"]').on('click', accept_session_action);
    $('.menu-button,.menu-dual-button [role="main"],.menu-dual-button [role="sub"]').hover(face_menu_hovering);
    /* Keyboard controls */
    // Universal
    $('.header [role="value"]').on('keypress', universal_accepter_on_enter);
    $('#tabs_block').on('keydown', universal_back_on_backspace);
    // Session block
    $('#sessions_block').on('keydown', session_block_keybindings);
    // Face menu controls
    $('#face').on('keydown', $face_menu_keybindings.handle);
    $('#face').focus();
}, 100);



