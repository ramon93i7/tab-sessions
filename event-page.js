function create_tab(url) {
    return new Promise((resolve, reject) => {
        chrome.tabs.create({'url': url}, tab => resolve(tab));
    });
}

function create_tabs(urls) {
    return Promise.all(urls.map(create_tab))
}
